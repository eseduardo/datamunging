﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Weather.BL
{
    public static class Parser
    {
        public static List<Weather> Map(string[] data)
        {
            var l = new List<Weather>();
            foreach (var record in data)
            {

                var w = Map(record);
                if (w != null)
                    l.Add(w);
            }
            return l;
        }

        private static Weather Map(string record)
        {
            try
            {

            var columns = record.Split(' ');
            var data = columns.Where(c => !string.IsNullOrWhiteSpace(c)).Select(c => c).ToArray();
            return new Weather
            {
                Dy = data[0].CleanRubish("*").ToNumeric(),
                MxT = data[1].CleanRubish("*").ToNumeric(),
                MnT = data[2].CleanRubish("*").ToNumeric(),
            };
            }
            catch (Exception)
            {
                return null;
            }
        }
    }

public static class StringHelper
{
    public static int ToNumeric(this string value)
    {
        return Convert.ToInt32(value);
    }

    public static string CleanRubish(this string value, string badCharacter)
    {
        return value.Replace(badCharacter, "");
    }
}

    public class Weather
    {
        public int Dy { get; set; }
        public int MxT { get; set; }
        public int MnT { get; set; }
        public int DifferenceT()
        {
            return MxT - MnT;
        }
    }    //Dy MxT MnT
}